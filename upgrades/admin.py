"""Admin for upgrades."""

import logging

from django.contrib import admin            # pylint:disable=import-error
from django.http import HttpResponse        # pylint:disable=import-error

# Register your models here.
from .models import Element
from .models import Upgradable
from .models import Upgrade
from .models import UpgradeElements
from .models import UpgradeMaterials


logger = logging.getLogger(__name__)        # pylint:disable=invalid-name


class UpgradeElementsInline(admin.TabularInline):
    # pylint:disable=too-few-public-methods

    """Inline rows for the list of elements."""

    model = UpgradeElements
    extra = 3
    fields = ('quantity', 'element')


class UpgradeMaterialsInline(admin.TabularInline):
    # pylint:disable=too-few-public-methods

    """Inline rows for upgrades necessary for an upgrade."""

    model = UpgradeMaterials
    extra = 1
    fk_name = 'upgrade'
    fields = ('quantity', 'material')


class UpgradeAdmin(admin.ModelAdmin):
    # pylint:disable=too-few-public-methods

    """Admin for upgrades."""

    inlines = [UpgradeElementsInline, UpgradeMaterialsInline]
    actions = ['export_as_dot']

    def export_as_dot(self, _, queryset):
        """Export the selected elements as a DOT graph."""
        # pylint:disable=no-self-use
        content = []
        content.append('digraph nomanssky {')
        content.append('\trankdir="LR";')
        content.append('')

        element_list = {}

        for upgrade in queryset:
            logger.debug('Dot for %s:', upgrade)
            content.append(upgrade.to_dot())
            content.append('')

            for element in UpgradeElements.objects.filter(upgrade=upgrade):
                logger.debug('\tUpgrade requires %s', element.element)
                if str(element.element) not in element_list:
                    logger.debug('\t\tAdded')
                    element_list[str(element.element)] = \
                        element.element.to_dot()

        content.append('')
        content.append('/* elements */')

        for element in element_list:
            content.append(element_list[element])
            content.append('')

        content.append('}')

        response = HttpResponse('\n'.join(content),
                                content_type='text/plain')
        return response
    export_as_dot.short_description = 'Export to DOT'


admin.site.register(Element)
admin.site.register(Upgradable)
admin.site.register(Upgrade, UpgradeAdmin)
