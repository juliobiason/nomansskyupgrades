"""Models for the upgrades."""

from __future__ import unicode_literals

from django.db import models                    # pylint:disable=import-error
from django.utils.text import slugify           # pylint:disable=import-error

# pylint:disable=too-few-public-methods


def slug(name):
    """Convert a name to a slug that can be used to dot."""
    return slugify(name).replace('-', '_')


class SluggedName(models.Model):

    """Abstract model with a name and a slug."""

    name = models.CharField(max_length=30)

    @property
    def slug(self):
        """Slug representation of the record."""
        return slug(self.name)

    class Meta:
        # pylint:disable=missing-docstring
        # pylint:disable=old-style-class
        # pylint:disable=no-init
        abstract = True

    objects = models.Manager()

    def __str__(self):
        return self.name


class Element(SluggedName):

    """A minerable element"""

    def to_dot(self):
        """Return a representation of this element in dot format."""
        table = ['<TABLE>']

        first = True
        for upgrades in UpgradeElements.objects.filter(element=self):
            table.append('<TR>')
            table.append('<TD PORT="{upgrade}">{quantity}</TD>'.format(
                upgrade=upgrades.upgrade.slug,
                quantity=upgrades.quantity))

            if first:
                table.append('<TD ROWSPAN="{rows}">{name}</TD>'.format(
                    rows=UpgradeElements.objects.filter(element=self).count(),
                    name=self.name))
                first = False
            table.append('</TR>')
        table.append('</TABLE>')

        return '{slug} [shape=none, label=<{table}>];'.format(
            slug=self.slug,
            table='\n'.join(table))


class Upgradable(SluggedName):

    """An upgradable part."""

    def to_dot(self):
        """Return the upgradable representation in dot format."""
        return '{slug} [shape=record, label="{name}"];'.format(
            slug=self.slug,
            name=self.name)


class Upgrade(SluggedName):

    """An upgrade"""

    upgradable_part = models.ForeignKey(Upgradable)

    def __str__(self):
        return '{upgrade} ({part})'.format(
            part=self.upgradable_part.name,
            upgrade=self.name)

    def to_dot(self):
        """Conver the upgrade repretation to dot format."""
        content = ['/* upgrade group  */']
        content.append('{group} -> {this};'.format(
            group=self.upgradable_part.slug,
            this=self.slug))

        content.append('')
        content.append('/* Upgrade materials */')
        content.append('{slug} [label="{name}"];'.format(
            slug=self.slug,
            name=self.name))

        for element in UpgradeElements.objects.filter(upgrade=self):
            content.append('{slug} -> {element}:{slug};'.format(
                slug=self.slug,
                element=element.element.slug))

        for material in UpgradeMaterials.objects.filter(upgrade=self):
            content.append('{slug} -> {material_slug};'.format(
                slug=self.slug,
                material_slug=material.material.slug))

        return '\n'.join(content)


class UpgradeElements(models.Model):

    """List of elements necessary to build an upgrade."""

    upgrade = models.ForeignKey(Upgrade)
    element = models.ForeignKey(Element)
    quantity = models.IntegerField()

    objects = models.Manager()


class UpgradeMaterials(models.Model):

    """List of previous upgrades necessary to build an upgrade."""

    upgrade = models.ForeignKey(Upgrade)
    material = models.ForeignKey(Upgrade, related_name='upgrade_material')
    quantity = models.IntegerField()

    objects = models.Manager()
